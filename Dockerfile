# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

FROM ubuntu:20.04

MAINTAINER Josh Granek "josh@duke.edu"

USER root

# Install all OS dependencies for notebook server that starts but lacks all
# features (e.g., download as all possible file formats)
ENV DEBIAN_FRONTEND noninteractive
ENV R_VERSION="4.1.0"
ENV BIOCONDUCTOR_VERSION="3.13"
ENV CRAN_REPO="'https://mran.revolutionanalytics.com/snapshot/2021-05-19'"


# get R from a CRAN archive 
RUN apt-get update && \
    apt-get -yq --no-install-recommends install \
   gnupg2
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
RUN apt-get update && \
    apt-get dist-upgrade -yq 

RUN apt-get update && \
    apt-get install -yq --no-install-recommends \
    wget \
    bzip2 \
    less \
    make \
    ca-certificates \
    sudo \
    locales \
    git \
    ssh \
    vim \
    jed \
    emacs \
    xclip \
    build-essential \
    python-dev \
    python \
    python3-dev \
    python3 \
    unzip \
    libsm6 \
    pandoc \
    pkg-config \
    libxrender1 \
    inkscape \
    rsync \
    gzip \
    tar \
    python3-pip \
    apt-utils \
    curl \
    libxml2-dev \
    libgsl0-dev \
    fastqc default-jre \
    circos \
    parallel \
    time \
    htop \
    rna-star \
    bwa \
    samtools \
    tabix \
    picard-tools \
    openjdk-11-jdk \
    openjdk-11-jre \
    bcftools \
    bedtools \
    vcftools \
    seqtk \
    lftp \
    trimmomatic
    # sra-toolkit \

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

ENV SHELL /bin/bash
ENV NB_USER jovyan
ENV NB_UID 1000
ENV HOME /home/$NB_USER
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV PATH $PATH:/opt/bin:$HOME/.local/bin


# Create jovyan user with UID=1000 and in the 'users' group
RUN useradd -m -s /bin/bash -N -u $NB_UID $NB_USER

USER $NB_USER

# Setup jovyan home directory
RUN mkdir /home/$NB_USER/work && \
    mkdir /home/$NB_USER/.jupyter && \
    mkdir /home/$NB_USER/.ssh && \
    mkdir -p /home/$NB_USER/.local/share/jupyter/runtime && \
    printf "Host gitlab.oit.duke.edu \n \t IdentityFile ~/work/.HTSgitlab.key\n"  > /home/$NB_USER/.ssh/config && \
    echo "cacert=/etc/ssl/certs/ca-certificates.crt" > /home/$NB_USER/.curlrc

USER root

RUN pip3 install --no-cache-dir --upgrade setuptools
RUN pip3 install --no-cache-dir \
  wheel \
  jupyter \
  ipywidgets \
  pysam \
  biopython \
  DukeDSClient \
  multiqc \
  jupytext \
  RSeQC

RUN pip3 install --no-cache-dir bash_kernel && python3 -m bash_kernel.install

# downgrade matplotlib for multiqc
#RUN pip3 uninstall --yes matplotlib && \
#    pip3 install --no-cache-dir 'matplotlib==2.2.3'

    
# USER root    
# Activate ipywidgets extension in the environment that runs the notebook server
RUN jupyter nbextension enable --py widgetsnbextension --sys-prefix
# RUN ipcluster nbextension  enable --user


USER $NB_USER

# Configure ipython kernel to use matplotlib inline backend by default
RUN mkdir -p $HOME/.ipython/profile_default/startup
COPY mplimporthook.py $HOME/.ipython/profile_default/startup/

#----------- end scipy

#----------- datascience
USER root


RUN apt-get update && \
    apt-get install dirmngr -yq --install-recommends && \
    apt-get install software-properties-common -yq && \
    apt-get install apt-transport-https -yq

# Add cran repo

RUN echo "deb http://cran.r-project.org/bin/linux/ubuntu focal-cran40/" > /etc/apt/sources.list.d/r.list && \
    add-apt-repository 'deb http://cran.r-project.org/bin/linux/ubuntu focal-cran40/'

RUN apt-get update && \
    apt-get -yq --no-install-recommends install \
    r-base=${R_VERSION}* \
    r-base-core=${R_VERSION}* \
    r-base-dev=${R_VERSION}* \
    r-mathlib=${R_VERSION}* \
    r-recommended=${R_VERSION}* \
    r-base-html=${R_VERSION}* \
    r-doc-html=${R_VERSION}* \
    libcurl4-openssl-dev \
    libssl-dev \
    libxml2-dev \
    libcairo2-dev \
    libxt-dev

RUN Rscript -e "install.packages(c('IRkernel','plyr','devtools','RCurl','curl','tidyverse','shiny','rmarkdown', 'forecast', 'RSQLite', 'reshape2', 'nycflights13','caret', 'crayon', 'randomforest', 'htmltools','sparklyr', 'htmlwidgets', 'hexbin','ROCR','mvtnorm','pheatmap','formatR','dendextend', 'rentrez','R.utils','ashr'), repos = ${CRAN_REPO})"

RUN Rscript -e "IRkernel::installspec(user = FALSE)"

RUN Rscript -e "if (!requireNamespace('BiocManager', quietly = TRUE)) install.packages('BiocManager', repos = ${CRAN_REPO}); \
                   BiocManager::install()"

RUN Rscript -e "BiocManager::install(version=${BIOCONDUCTOR_VERSION}, pkgs=c('golubEsets','multtest','qvalue','limma','gage','pheatmap', 'ggbio', 'ShortRead', 'DESeq2', 'dada2', 'KEGG.db', 'pwr','RColorBrewer','GSA','dendextend','pheatmap','cgdsr', 'caret', 'ROCR','genefilter','GEOquery', 'airway', 'pathview','Gviz','phyloseq','plyranges','fgsea'))"

# install fastq-mcf and fastq-multx from source since apt-get install causes problems
RUN mkdir -p /usr/bin && \
    	  cd /tmp && \
	  wget https://github.com/ExpressionAnalysis/ea-utils/archive/1.04.807.tar.gz && \
	  tar -zxf 1.04.807.tar.gz &&  \
    	  cd ea-utils-1.04.807/clipper &&  \
    	  make fastq-mcf fastq-multx &&  \
    	  cp fastq-mcf fastq-multx /usr/bin &&  \
    	  cd /tmp &&  \
    	  rm -rf ea-utils-1.04.807

# Install Newer SRA toolkits than available from Ubuntu <https://github.com/ncbi/sra-tools/wiki/01.-Downloading-SRA-Toolkit>
RUN curl -L -s https://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/2.11.0/sratoolkit.2.11.0-ubuntu64.tar.gz | tar -zx && \
  mkdir -p /opt && \
  mv sratoolkit.2.11.0-ubuntu64/bin sratoolkit.2.11.0-ubuntu64/schema /opt && \
  rm -rf sratoolkit.2.11.0-ubuntu64

#----------- end datascience

#----------- notebook
EXPOSE 8888
WORKDIR /home/$NB_USER/work

# tini init

ENV TINI_VERSION v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]


# Configure container startup

CMD ["/usr/local/bin/start-notebook.sh"]

# Add local files as late as possible to avoid cache busting
COPY start.sh /usr/local/bin/
COPY start-notebook.sh /usr/local/bin/
COPY start-singleuser.sh /usr/local/bin/
COPY jupyter_notebook_config.py /home/$NB_USER/.jupyter/
RUN chown -R $NB_USER:users /home/$NB_USER/.jupyter

USER $NB_USER

  
  
# Setup up git auto-completion based on https://git-scm.com/book/en/v1/Git-Basics-Tips-and-Tricks#Auto-Completion
# RUN wget --directory-prefix /etc/bash_completion.d/ \
#      https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash

# directories to hold data for the students and a common shared space

# UNDER CONSTRUCTION: Nerd Work Zone >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# USER $NB_USER
# UNDER CONSTRUCTION: Nerd Work Zone <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

#######
USER root

# Clean up 
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir /data /sharedspace 
RUN chown jovyan /data /sharedspace 

# Switch back to jovyan to avoid accidental container runs as root
USER jovyan
